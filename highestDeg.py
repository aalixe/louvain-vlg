#! /usr/bin/python3
import sys


fin = open(sys.argv[1])

is_node = False
counter = 0
max = 0
maxid = 0


for l in fin:
    spl = l.split()
    if len(spl) != 2 :
        continue
    if spl[0] == "0" and not is_node :
        counter += 1
        if counter == 2:
            is_node = True
    if not is_node:
        if int(spl[1]) > max :
            max = int(spl[1])
            maxid = spl[0]
    if is_node:
        break

fin.close()

print(maxid)
